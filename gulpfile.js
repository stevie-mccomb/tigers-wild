var gulp = require('gulp');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var gif = require('gulp-if');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

var argv = require('yargs').argv;

gulp.task('default', ['minify', 'sassify']);

gulp.task('minify', function () {
	gulp.src([
			'assets/js/GameObject.js',
			'assets/js/Controller.js',
			'assets/js/Sprite.js',
			'assets/js/Stage.js',
			'assets/js/Game.js',
		])
		.pipe(plumber())
		.pipe(babel({
            presets: ['es2015']
        }))
		.pipe(gif(argv.production, uglify()))
		.pipe(concat('app.js'))
		.pipe(gulp.dest('public/js'));
});

gulp.task('sassify', function () {
	gulp.src('assets/sass/**/*')
		.pipe(plumber())
		.pipe(gif(argv.production, sass({ outputStyle: 'compressed' }), sass()))
		.pipe(concat('app.css'))
		.pipe(gulp.dest('public/css'));
});

gulp.task('watch', ['default'], function () {
	gulp.watch('assets/js/**/*', ['minify']);
	gulp.watch('assets/sass/**/*', ['sassify']);
});
