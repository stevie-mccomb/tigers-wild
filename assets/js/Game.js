class Game
{
    constructor()
    {
        Game.instance = this;

        this.element = document.createElement('div');
        document.body.appendChild(this.element);
        this.element.setAttribute('class', 'game');

        this.lastUpdated = 0;

        new Stage();

        requestAnimationFrame(this.loop.bind(this));
    }
    
    loop(timestamp)
    {
        var deltaTime = timestamp - this.lastUpdated;

        if (!this.paused) {
            this.update(deltaTime);
            this.redraw(deltaTime);
        }

        this.lastUpdated = timestamp;
    }

    update(deltaTime)
    {
        Stage.instance.update(deltaTime);

        for (var i = 0; i < GameObject.instances.length; ++i) {
            GameObject.instances[i].update(deltaTime);
        }
    }

    redraw(deltaTime)
    {
        Stage.instance.redraw(deltaTime);

        for (var i = 0; i < GameObject.instances.length; ++i) {
            GameObject.instances[i].redraw(deltaTime);
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {
    new Game();
});
