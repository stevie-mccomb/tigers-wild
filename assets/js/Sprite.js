class Sprite
{
    constructor(entity, src, options)
    {
        if (!options) options = {};

        this.entity = entity;

        this.image = new Image();
        this.image.src = src;

        this.tickCount = 0;
        this.ticksPerFrame = options.ticksPerFrame || 1;

        this.frameIndex = options.frameIndex || 0;
        this.animationIndex = options.animationIndex || 0;

        this.animations = options.animations || [{ name: 'idle', frames: [{ width: this.entity.width, height: this.entity.height, x: 0, y: 0 }] }];

        this.loop = options.loop !== undefined ? options.loop : true;
        this.paused = options.paused !== undefined ? options.paused : false;
    }

    update(deltaTime)
    {
        if (this.paused) return false;

        ++this.tickCount;

        if (this.tickCount >= this.ticksPerFrame) {
            this.tickCount = 0;

            ++this.frameIndex;

            if (this.frameIndex >= this.animation.frames.length - 1) {
                if (this.loop) {
                    this.frameIndex = 0;
                } else {
                    --this.frameIndex;
                    this.paused = true;
                }
            }
        }
    }

    redraw(deltaTime)
    {
        Stage.instance.context.drawImage(
            this.image,
            this.frame.x,
            this.frame.y,
            this.frame.width,
            this.frame.height,
            this.entity.x,
            this.entity.y,
            this.frame.width,
            this.frame.height
        );
    }

    get animation()
    {
        return this.animations[this.animationIndex];
    }

    get frame()
    {
        return this.animation.frames[this.frameIndex];
    }
}
