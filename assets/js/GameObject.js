class GameObject
{
    constructor()
    {
        GameObject.instances.push(this);

        this.width = 0;
        this.height = 0;

        this.x = 0;
        this.y = 0;
    }

    update(deltaTime)
    {
        //
    }

    redraw(deltaTime)
    {
        if (this.sprite) {
            this.sprite.update(deltaTime);
            this.sprite.redraw(deltaTime);
        }
    }

    get top()
    {
        return this.y;
    }

    get left()
    {
        return this.x;
    }

    get bottom()
    {
        return this.top + this.height;
    }

    get right()
    {
        return this.left + this.width;
    }

    set top(value)
    {
        this.y = value;
    }

    set left(value)
    {
        this.x = value;
    }

    set bottom(value)
    {
        this.y = value - this.height;
    }

    set right(value)
    {
        this.x = value - this.width;
    }
}

GameObject.instances = [];
