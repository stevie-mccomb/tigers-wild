class Controller
{
    constructor()
    {
        Controller.instance = this;

        this.keys = [];
        this.downListeners = [];
        this.upListeners = [];

        document.addEventListener('keydown', this.keydownHandler.bind(this));
        document.addEventListener('keyup', this.keyupHandler.bind(this));
    }

    keydownHandler(e)
    {
        var key = e.key;
        var index = this.keys.indexOf(key);

        if (index < 0) {
            this.keys.push(key);
        }
    }

    keyupHandler(e)
    {
        var key = e.key;
        var index = this.keys.indexOf(key);

        if (index >= 0) {
            this.keys.splice(index, 1);
        }
    }

    onDown(key, callback)
    {
        this.downListeners.push({
            key: key,
            callback: callback
        });
    }

    onUp(key, callback)
    {
        this.upListeners.push({
            key: key,
            callback: callback
        });
    }

    offDown(key, callback)
    {
        for (var i = this.downListeners.length - 1; i >= 0; --i) {
            if (this.downListeners[i].key === key) {
                if (callback) {
                    if (this.downListeners[i].callback === callback) {
                        this.downListeners.splice(i, 1);
                    }
                } else {
                    this.downListeners.splice(i, 1);
                }
            }
        }
    }

    offUp(key, callback)
    {
        for (var i = this.upListeners.length - 1; i >= 0; --i) {
            if (this.upListeners[i].key === key) {
                if (callback) {
                    if (this.upListeners[i].callback === callback) {
                        this.upListeners.splice(i, 1);
                    }
                } else {
                    this.upListeners.splice(i, 1);
                }
            }
        }
    }
}
