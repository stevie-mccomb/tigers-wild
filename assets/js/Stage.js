class Stage
{
    constructor()
    {
        Stage.instance = this;

        this.element = document.createElement('canvas');
        Game.instance.element.appendChild(this.element);

        this.context = this.element.getContext('2d');

        this.resize();

        window.addEventListener('resize', this.resize.bind(this));
    }

    resize()
    {
        this.element.setAttribute('width', this.width);
        this.element.setAttribute('height', this.height);
    }

    update(deltaTime)
    {
        //
    }

    redraw(deltaTime)
    {
        this.context.clearRect(this.left, this.top, this.width, this.height);
    }

    get width()
    {
        return this.element.offsetWidth;
    }

    get height()
    {
        return this.element.offsetHeight;
    }

    get top()
    {
        return 0;
    }

    get left()
    {
        return 0;
    }

    get bottom()
    {
        return this.top + this.height;
    }

    get right()
    {
        return this.left + this.width;
    }
}
