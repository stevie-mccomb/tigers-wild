class RedTiger extends Tiger
{
    constructor()
    {
        super();

        this.width = 32;
        this.height = 32;

        this.sprite = new Sprite(this, 'tiger_red.png', {
            ticksPerFrame: 6
        });
    }
}
