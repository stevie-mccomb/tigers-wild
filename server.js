var express = require('express');
var http = require('http');
var socketIO = require('socket.io');

var app = express();
var server = http.Server(app);
var io = socketIO(server);

app.use(express.static('public'));

io.on('connection', function (socket) {
	console.log(socket.id + ' connected...');

	socket.on('disconnect', function (message) {
		console.log(socket.id + ' disconnected...');
	});
});

server.listen(3000, function () {
	console.log('Listening on *:3000');
});
