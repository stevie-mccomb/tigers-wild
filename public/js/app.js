"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GameObject = function () {
    function GameObject() {
        _classCallCheck(this, GameObject);

        GameObject.instances.push(this);

        this.width = 0;
        this.height = 0;

        this.x = 0;
        this.y = 0;
    }

    _createClass(GameObject, [{
        key: "update",
        value: function update(deltaTime) {
            //
        }
    }, {
        key: "redraw",
        value: function redraw(deltaTime) {
            if (this.sprite) {
                this.sprite.update(deltaTime);
                this.sprite.redraw(deltaTime);
            }
        }
    }, {
        key: "top",
        get: function get() {
            return this.y;
        },
        set: function set(value) {
            this.y = value;
        }
    }, {
        key: "left",
        get: function get() {
            return this.x;
        },
        set: function set(value) {
            this.x = value;
        }
    }, {
        key: "bottom",
        get: function get() {
            return this.top + this.height;
        },
        set: function set(value) {
            this.y = value - this.height;
        }
    }, {
        key: "right",
        get: function get() {
            return this.left + this.width;
        },
        set: function set(value) {
            this.x = value - this.width;
        }
    }]);

    return GameObject;
}();

GameObject.instances = [];
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Controller = function () {
    function Controller() {
        _classCallCheck(this, Controller);

        Controller.instance = this;

        this.keys = [];
        this.downListeners = [];
        this.upListeners = [];

        document.addEventListener('keydown', this.keydownHandler.bind(this));
        document.addEventListener('keyup', this.keyupHandler.bind(this));
    }

    _createClass(Controller, [{
        key: 'keydownHandler',
        value: function keydownHandler(e) {
            var key = e.key;
            var index = this.keys.indexOf(key);

            if (index < 0) {
                this.keys.push(key);
            }
        }
    }, {
        key: 'keyupHandler',
        value: function keyupHandler(e) {
            var key = e.key;
            var index = this.keys.indexOf(key);

            if (index >= 0) {
                this.keys.splice(index, 1);
            }
        }
    }, {
        key: 'onDown',
        value: function onDown(key, callback) {
            this.downListeners.push({
                key: key,
                callback: callback
            });
        }
    }, {
        key: 'onUp',
        value: function onUp(key, callback) {
            this.upListeners.push({
                key: key,
                callback: callback
            });
        }
    }, {
        key: 'offDown',
        value: function offDown(key, callback) {
            for (var i = this.downListeners.length - 1; i >= 0; --i) {
                if (this.downListeners[i].key === key) {
                    if (callback) {
                        if (this.downListeners[i].callback === callback) {
                            this.downListeners.splice(i, 1);
                        }
                    } else {
                        this.downListeners.splice(i, 1);
                    }
                }
            }
        }
    }, {
        key: 'offUp',
        value: function offUp(key, callback) {
            for (var i = this.upListeners.length - 1; i >= 0; --i) {
                if (this.upListeners[i].key === key) {
                    if (callback) {
                        if (this.upListeners[i].callback === callback) {
                            this.upListeners.splice(i, 1);
                        }
                    } else {
                        this.upListeners.splice(i, 1);
                    }
                }
            }
        }
    }]);

    return Controller;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Sprite = function () {
    function Sprite(entity, src, options) {
        _classCallCheck(this, Sprite);

        if (!options) options = {};

        this.entity = entity;

        this.image = new Image();
        this.image.src = src;

        this.tickCount = 0;
        this.ticksPerFrame = options.ticksPerFrame || 1;

        this.frameIndex = options.frameIndex || 0;
        this.animationIndex = options.animationIndex || 0;

        this.animations = options.animations || [{ name: 'idle', frames: [{ width: this.entity.width, height: this.entity.height, x: 0, y: 0 }] }];

        this.loop = options.loop !== undefined ? options.loop : true;
        this.paused = options.paused !== undefined ? options.paused : false;
    }

    _createClass(Sprite, [{
        key: 'update',
        value: function update(deltaTime) {
            if (this.paused) return false;

            ++this.tickCount;

            if (this.tickCount >= this.ticksPerFrame) {
                this.tickCount = 0;

                ++this.frameIndex;

                if (this.frameIndex >= this.animation.frames.length - 1) {
                    if (this.loop) {
                        this.frameIndex = 0;
                    } else {
                        --this.frameIndex;
                        this.paused = true;
                    }
                }
            }
        }
    }, {
        key: 'redraw',
        value: function redraw(deltaTime) {
            Stage.instance.context.drawImage(this.image, this.frame.x, this.frame.y, this.frame.width, this.frame.height, this.entity.x, this.entity.y, this.frame.width, this.frame.height);
        }
    }, {
        key: 'animation',
        get: function get() {
            return this.animations[this.animationIndex];
        }
    }, {
        key: 'frame',
        get: function get() {
            return this.animation.frames[this.frameIndex];
        }
    }]);

    return Sprite;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stage = function () {
    function Stage() {
        _classCallCheck(this, Stage);

        Stage.instance = this;

        this.element = document.createElement('canvas');
        Game.instance.element.appendChild(this.element);

        this.context = this.element.getContext('2d');

        this.resize();

        window.addEventListener('resize', this.resize.bind(this));
    }

    _createClass(Stage, [{
        key: 'resize',
        value: function resize() {
            this.element.setAttribute('width', this.width);
            this.element.setAttribute('height', this.height);
        }
    }, {
        key: 'update',
        value: function update(deltaTime) {
            //
        }
    }, {
        key: 'redraw',
        value: function redraw(deltaTime) {
            this.context.clearRect(this.left, this.top, this.width, this.height);
        }
    }, {
        key: 'width',
        get: function get() {
            return this.element.offsetWidth;
        }
    }, {
        key: 'height',
        get: function get() {
            return this.element.offsetHeight;
        }
    }, {
        key: 'top',
        get: function get() {
            return 0;
        }
    }, {
        key: 'left',
        get: function get() {
            return 0;
        }
    }, {
        key: 'bottom',
        get: function get() {
            return this.top + this.height;
        }
    }, {
        key: 'right',
        get: function get() {
            return this.left + this.width;
        }
    }]);

    return Stage;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Game = function () {
    function Game() {
        _classCallCheck(this, Game);

        Game.instance = this;

        this.element = document.createElement('div');
        document.body.appendChild(this.element);
        this.element.setAttribute('class', 'game');

        this.lastUpdated = 0;

        new Stage();

        requestAnimationFrame(this.loop.bind(this));
    }

    _createClass(Game, [{
        key: 'loop',
        value: function loop(timestamp) {
            var deltaTime = timestamp - this.lastUpdated;

            if (!this.paused) {
                this.update(deltaTime);
                this.redraw(deltaTime);
            }

            this.lastUpdated = timestamp;
        }
    }, {
        key: 'update',
        value: function update(deltaTime) {
            Stage.instance.update(deltaTime);

            for (var i = 0; i < GameObject.instances.length; ++i) {
                GameObject.instances[i].update(deltaTime);
            }
        }
    }, {
        key: 'redraw',
        value: function redraw(deltaTime) {
            Stage.instance.redraw(deltaTime);

            for (var i = 0; i < GameObject.instances.length; ++i) {
                GameObject.instances[i].redraw(deltaTime);
            }
        }
    }]);

    return Game;
}();

document.addEventListener('DOMContentLoaded', function () {
    new Game();
});